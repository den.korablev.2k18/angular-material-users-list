export interface User {
    id: string;
    name: string;
    surName: string;
    date_birth: string;
    job: string;
    education: string;
    car: boolean;
    phone_number: number;
    years: number;
}
