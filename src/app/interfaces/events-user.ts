import { User } from "./user";

export interface EventsUser {
    create: boolean;
    user: User
}
