import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { DialogComponent } from 'src/app/components/dialog/dialog.component';
import { EventsUser } from 'src/app/interfaces/events-user';
import { User } from 'src/app/interfaces/user';
import { DialogService } from 'src/app/shared/services/dialog.service';
import { UsersService } from 'src/app/shared/services/users.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent {
  public user?: User;
  public userId!: string;

  constructor(
    private activeRoute: ActivatedRoute, 
    private usersService: UsersService, 
    public dialog: MatDialog, 
    private route: Router, 
    private dialogService: DialogService
  ) {

    activeRoute.params.subscribe(params => {
      this.userId = params['id'];

      this.usersService.readById(params['id']).subscribe((user: User) => {
        this.user = user;
      });

    });

   }


  public editUser(user: User): void {
    this.dialogService.editUser(user, (data: EventsUser) => {
      if(data){
        this.user = data.user;
      }
    });
  }

  public deleteUser(id: string, name: string): void {
    this.dialogService.deleteUser(id, name, (user: User) => {
      if(user.id){
        this.route.navigate(['list-users']);
      }
    });
  }
}
