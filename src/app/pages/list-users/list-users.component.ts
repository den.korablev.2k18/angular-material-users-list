import { Component, OnDestroy, OnInit } from '@angular/core';
import { User } from 'src/app/interfaces/user';
import { UsersService } from 'src/app/shared/services/users.service';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { DialogComponent } from 'src/app/components/dialog/dialog.component';
import { EventsUser } from 'src/app/interfaces/events-user';
import { DialogService } from 'src/app/shared/services/dialog.service';

@Component({
  selector: 'app-list-users',
  templateUrl: './list-users.component.html',
  styleUrls: ['./list-users.component.scss'],
})
export class ListUsersComponent {

  public users!: User[];

  constructor(
    private usersService: UsersService, 
    private route: Router, 
    public dialog: MatDialog, 
    private dialogService: DialogService
  ) 
  {
    this.getUsers();
   }

  public getUsers(): void {
    this.usersService.read().subscribe((users: User[]) => {
      this.users = users;
    });
  }


  public showUser(id: string): void {
    this.route.navigate(['list-users', id]);
  }

  public editUser(user: User): void {
    this.dialogService.editUser(user, (data: EventsUser) => {
      const indexUser = this.users.findIndex((u: User) => u.id === data?.user.id);
      if(data){
        this.users[indexUser] = data.user;  
      }
    });
  }

  public deleteUser(id: string, name: string): void {
    this.dialogService.deleteUser(id, name, (user: User) => {
      if(user.id){
        this.users = this.users.filter((u: User) => u.id !== id);
      }
    });
  }

  public create(): void {
    this.dialog.open(DialogComponent, {
      width: '250px',
      data:{
        create: true,
        user: null
      }
    }).afterClosed().subscribe((data : EventsUser) => {
      if(data){
        this.users.push(data.user);
      }
    });
  }
}
