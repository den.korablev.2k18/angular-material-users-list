import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatSelectModule } from '@angular/material/select';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDialogModule } from '@angular/material/dialog'
import  { MatListModule } from '@angular/material/list';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MAT_DATE_LOCALE, MatNativeDateModule } from '@angular/material/core';
import { UsersService } from './services/users.service';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MatListModule,
    MatButtonModule,
    MatIconModule,

    MatCardModule,
    MatDialogModule,

    MatFormFieldModule,
    MatInputModule,

    MatAutocompleteModule,
    MatSelectModule,

    MatDatepickerModule,
    MatNativeDateModule
  ],
  exports:[
    CommonModule,
    MatListModule,
    MatButtonModule,
    MatIconModule,

    MatCardModule,
    MatDialogModule,

    MatFormFieldModule,
    MatInputModule,

    MatAutocompleteModule,
    MatSelectModule,

    MatDatepickerModule,
    MatNativeDateModule
  ],

  providers: [UsersService, { provide: MAT_DATE_LOCALE, useValue: 'en-GB' }]
})
export class SharedModule { }
