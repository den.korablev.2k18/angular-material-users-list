import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmComponent } from 'src/app/components/confirm/confirm.component';
import { DialogComponent } from 'src/app/components/dialog/dialog.component';
import { EventsUser } from 'src/app/interfaces/events-user';
import { User } from 'src/app/interfaces/user';
import { UsersService } from './users.service';
import { IsConfirm } from 'src/app/interfaces/is-confirm';

@Injectable({
  providedIn: 'root'
})
export class DialogService {

  constructor(public dialog: MatDialog, private usersService: UsersService) { }


  public editUser(user: User, callback: Function): void {
    this.dialog.open(DialogComponent, {
      width: '250px',
      data: {
        create: false,
        user: user
      }
    }).afterClosed().subscribe((data: EventsUser) => {
      callback(data);
    });
  }

  public deleteUser(id: string, name: string, callback: Function): void {
    this.dialog.open(ConfirmComponent,{
      width: '250px',
      data: {
        name: name
      }
    }).afterClosed().subscribe((data: IsConfirm) => {
      if(data?.isConfirm){
        this.usersService.delete(id).subscribe((user: User) => {
          callback(user);
        });
      }
    });
  }
}
