import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from '../../interfaces/user';

@Injectable()
export class UsersService {
  static url = 'https://65aa6ed4081bd82e1d96f136.mockapi.io/testDenis/users';

  constructor(private http: HttpClient) { }

  public read(): Observable<User[]> {
    return this.http.get<User[]>(UsersService.url);
  }

  public readById(id: string): Observable<User> {
    return this.http.get<User>(UsersService.url + '/' + id);
  }

  public create(user: User): Observable<User> {
    return this.http.post<User>(UsersService.url, user);
  }

  public update(user: User): Observable<User> {
    return this.http.put<User>(UsersService.url + '/' + user.id, user)
  }

  public delete(id: string): Observable<User> {
    return this.http.delete<User>(UsersService.url + '/' + id)
  }
}
