import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { User } from 'src/app/interfaces/user';

@Component({
  selector: 'app-confirm',
  templateUrl: './confirm.component.html',
  styleUrls: ['./confirm.component.scss']
})
export class ConfirmComponent {
  name!:string;
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: User, 
    public dialogRef: MatDialogRef<ConfirmComponent>
  ) 
  {
    this.name = this.data.name;
  }

   public closeDialog(isConfirm: boolean): void {
    this.dialogRef.close({
      isConfirm: isConfirm
    });
   }
}
