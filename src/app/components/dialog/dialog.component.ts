import { Component } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Inject } from '@angular/core';
import { User } from 'src/app/interfaces/user';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { UsersService } from 'src/app/shared/services/users.service';
import { EventsUser } from 'src/app/interfaces/events-user';

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss'],
})
export class DialogComponent {

  public formUser!: FormGroup;
  public user!: User;
  public educations: string[] = ['No', 'Colledge', 'University'];
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: EventsUser, 
    private usersService: UsersService, 
    public dialogRef: MatDialogRef<DialogComponent>
  ) 
  {
    this.user = this.data.user;

    this.formUser = new FormGroup({
      name: new FormControl(this.user?.name ? this.user.name : '', Validators.required),
      surName: new FormControl(this.user?.surName ? this.user.surName : '', Validators.required),
      phone_number: new FormControl(this.user?.phone_number ? this.user.phone_number : null, Validators.required),
      date_birth: new FormControl(this.user?.date_birth ? this.user.date_birth : '', Validators.required),
      years: new FormControl(this.user?.years ? this.user.years : '', Validators.required),
      education: new FormControl(this.user?.education ? this.user.education : '', Validators.required),
      job: new FormControl(this.user?.job ? this.user.job : '', Validators.required),
      car: new FormControl(this.user?.car ? this.user.car : false, Validators.required),
    });
   }

  public onSubmit(user: User): void {
    if(this.data.create){
      this.usersService.create(user).subscribe((user: User) => {
        if(user.id){
          this.dialogRef.close({
            create: this.data.create,
            user: user
          });
        }
      })
    } else {
      user.id = this.data.user.id;
      this.usersService.update(user).subscribe((user: User) => {
        if(user.id){
          this.dialogRef.close({
            create: this.data.create,
            user: user
          });
        }
      });
    }
  }

}
